package com.campusacademy.poketp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoketpApplication {

	public static void main(String[] args) {
		SpringApplication.run(PoketpApplication.class, args);
	}
}
